package junitdemo;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class Hello {

	@BeforeClass
	public static void callBeforeClass()
	{
		System.out.println("This method is called before test method");
	}
	
	@Before
	public void callBefore()
	{
		System.out.println("calling before method ");
	}
	

	@Test
	public void test() 
	{
		System.out.println("This is test method");		
	}
	
	@After
	public void callAfter()
	{
		System.out.println("This is class after");
	}

}
