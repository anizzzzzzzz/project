package junitdemo;

import java.util.Scanner;

public class PalindromeNumber 
{
	public static void main(String agrs[])
	{
		
		System.out.println("Enter a number");
		Scanner sc=new Scanner(System.in);
		int num=sc.nextInt();
		
		PalindromeNumber pn=new PalindromeNumber();
		
		int res=pn.checkPalindrome(num);
		
		if(num==res)
		{
			System.out.println(num+" is Palindrome Number");			
		}
		else
		{
			System.out.println(num+" is not Palindrome Number");
		}
	}
	
	public int checkPalindrome(int no)	{
		
		int rem=0,pal=0;
		
		while(no!=0)
		{
			rem=no%10;
			pal=pal*10+rem;
			no=no/10;
		}
		
		return pal;
	}

}
