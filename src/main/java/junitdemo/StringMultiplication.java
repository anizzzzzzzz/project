package junitdemo;

import java.util.Scanner;

public class StringMultiplication {

	public static void main(String[] args) {
		
		Scanner sc=new Scanner(System.in);
		
		System.out.println("Enter first Number");
		String s1=sc.nextLine();
		
		System.out.println("Enter second Number");
		String s2=sc.nextLine();
		
		char[] st1=s1.toCharArray();
		char[] st2=s1.toCharArray();
		
		int totalSum = 0, sum = 0;
		for (int i = 0; i < st1.length; i++){
			sum = 0;

			for (int j = 0; j < st2.length; j++){
				sum *= 10;
				sum += (st1[i] - '0') * (st2[j] - '0');
				
			}
			totalSum *=10;
			totalSum += sum;
		}
		
		System.out.println("Multiplication is "+totalSum);
		

	}

}
